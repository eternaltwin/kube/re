# Server commands

**Note:** Unless specified, all kube ids are 1-based; with `0` used to represent air.

## CWatch

Sent by the client to request data for an 8x8 zone chunk and register for chunk updates. 

**Parameters:**
- `mx: int`: x coordinate of the zone chunk (divided by 8)
- `my: int`: y coordinate of the zone chunk (divided by 8)


## CGenLevel

Sent by the client to provide data for missing 8x8 zone chunks, after receiving an `AGenerate`.

**Parameters:**
- `mx: int`: x coordinate of the zone chunk (divided by 8)
- `my: int`: y coordinate of the zone chunk (divided by 8)
- `data: haxe.io.Bytes`: raw kube data


## CSetBlocks

Sent by the client when a Kube was placed or taken.

**Parameters:**
- `data: [x, y, z, old, new]`:
    - `x: int`: the x coordinate of the modified kube
    - `y: int`: the y coordinate of the modified kube
    - `z: int`: the z coordinate of the modified kube
    - `old: int`: the kube currently present at this location
    - `new: int`: the new kube for this location


## CSavePos

Regularly sent by the client when the player is moving normally.

**Parameters:**
- `x: int`: x coordinate of the player, in 1/16ths of a kube
- `y: int`: y coordinate of the player, in 1/16ths of a kube
- `z: int`: z coordinate of the player, in 1/16ths of a kube
- `s: Null<int>`: used fraction of the water bar (0: full; 1000: empty)
    - TODO: how does this interact with the swimsuit upgrade?

## CAdminDelete

Delete a chunk (used by the map, does not work)

**Parameters:**
- `mx: int`: x coordinate of the zone chunk (divided by 8)
- `my: int`: y coordinate of the zone chunk (divided by 8)


## CLoad

Request data for an 8x8 zone chunk. (used by the map)

**Parameters:**
- `mx: int`: x coordinate of the zone chunk (divided by 8)
- `my: int`: y coordinate of the zone chunk (divided by 8)


## CRegen

Fill holes on the floor of a zone. Used to regenerate public zone ground. Not used by the client.

**Parameters:**
- `u: int`: universe
- `zx: int`: the x coordinate of the zone 
- `zy: int`: the y coordinate of the zone
- `block: int`: block id


## CGameOver

Sent by the client when the player dies.

**Parameters:**
- `g: _GO`: the way the player died: `_GO.GOWater` or `_GO.GOLava`

## CDoCheck

Sent by the client when completing a megakube.

**Parameters:**
- `px: int`: unknown
- `py: int`: unknown
- `pz: int`: unknown


## CDropKubes

Used to drop BNoel in the world.

**Parameters:**
- `zx1: int`: unknown
- `zy1: int`: unknown
- `zx2: int`: unknown
- `zy2: int`: unknown
- `qty: int`: unknown
- `k: int`: unknown
- `water: Boolean`: unknown
- `mine: Boolean`: unknown


## CPing

Regularly sent by the client to signal it's still alive.

**Parameters:**
- `i: int`: ping number, starts at 0 and increments for each successive ping


## CSavePhoto

Sent by the client to save the given image as a Kube photo.

**Parameters:**
- `zx: int`: the x coordinate of the zone from which the photo was taken
- `zy: int`: the y coordinate of the zone from which the photo was taken
- `big: String`: url of the picture
- `small: String`: url of the thumbnail
- `hide: Boolean`: photo coordinates hidden


## CActiveKube

Sent by the client when the player touches a special kube (forum, dolmen, kofre).

**Parameters:**
- `x: int`: x coordinate of the touched kube
- `y: int`: y coordinate of the touched kube
- `z: int`: z coordinate of the touched kube


## CUpdateTuto

Sent by the client when completing a tutorial step

**Parameters:**
- `v: int`: the ID of the completed step (or of the next step ? not sure)


## CGetStatus

Unknown (not used)

The server return ANothing.

**Parameters:**
- None


## CTeleport

Sent by the client when using a kube teleport.

**Parameters:**
- `x: int`: x coordinate of the player, in 1/16ths of a kube
- `y: int`: y coordinate of the player, in 1/16ths of a kube
- `z: int`: z coordinate of the player, in 1/16ths of a kube
- `s: Null<int>`: used fraction of the water bar (0: full; 1000: empty)
    - TODO: how does this interact with the swimsuit upgrade?

## CUndo

Sent by the client to undo the last action (kube placed or taken).

**Parameters:**
- None


## CCountBlocks

Ask the server to return the zone blocks count (except height 0 ?), and a (the count of water blocks at height 0 ?). Return `AValue`.

**Parameters:**
- `zu: int`: universe ?
- `zx: int`: the x coordinate of the zone 
- `zy: int`: the y coordinate of the zone


## CFix

Unknown. Return `AValue`.

(Maybe used to fix some errors like no water at height 0)

Not used by the client.

**Parameters:**
- `mu: int`: universe
- `mx: int`: x coordinate of the zone chunk (divided by 8)
- `my: int`: y coordinate of the zone chunk (divided by 8)


# Server answers

**Note:** Unless specified, all kube ids are 1-based; with `0` used to represent air.

## ABlocks

Sent to the client when a single kube was updated by the player (after `CSetBlocks` and `CUndo`).

**Parameters:**
- `a: [new]`:
    - `new: Null<int>`: the new id of the kube at the location (implicit from the corresponding `Cmd`)
        - TODO: understand the difference between `null` and `0`
        - TODO: sometimes `[undefined, null]` is returned, why?


## ASet

Sent to the client when a kube changed in the world (because another player placed it, or after opening a kofre or making a megakube).

**Parameters:**
- `x: int`: x coordinate of the kube which changed
- `y: int`: y coordinate of the kube which changed
- `z: int`: z coordinate of the kube which changed
- `k: int`: the id of the kube at this location


## APosSaved

Sent to the client to acknowledge its `CSavePos`.

**Parameters:**
- None


## AMap

Contains the zone data for an 8x8 zone chunk around the player.

**Parameters:**
- `mx: int`: x coordinate of the zone chunk (divided by 8)
- `my: int`: y coordinate of the zone chunk (divided by 8)
- `data: haxe.io.Bytes`: compressed (zlib) raw kube data, stored in (z, y, x) lexicographical order
- `patches: Null<haxe.io.Bytes>`: patches to apply to chunk data; each patch is made up of four bytes:
    - `x`: x coordinate inside the chunk
    - `y`: y coordinate inside the chunk
    - `z`: z coordinate inside the chunk
    - `k`: the kube id at the given location
- `zones: Array<Null<{ struct }>>`: metadata for the 8x8 zones of this chunk, stored
in (x, y) lexicographical order (if null, the zone is uninhabited)
    - `_d: Null<Date>`: date of location expiry (if null, this is a public zone)
    - `_n: Null<String>`: the name of the zone
    - `_u: Null<String>`: the name of the user owning this zone (if null, this is a public zone)
    - `_g: bool`: permission for the player to *get* kubes from this zone
    - `_p: bool`: permission for the player to *put* kubes in this zone


## AGenerate

Informs the client that there is no data for the given 8x8 zone chunk.

**Parameters:**
- `mx: int`: x coordinate of the zone chunk (divided by 8)
- `my: int`: y coordinate of the zone chunk (divided by 8)


## ARedirect

Sent to the client to redirect it to a new webpage (for example after a `CGameOver`).

**Parameters:**
- `url: String`: the URL to redirect to.


## AMessage

Sent to the client to display an ingame message.

**Parameters:**
- `text: String`: the text to display to the player
- `error: Boolean`: is this message an error?


## ANothing

Sent to the client to acknowledge its `CActiveKube`, if no `AMessage` was sent. (for ex. when opening a kofre)

**Parameters:**
- None


## AValue

Sent after a `CFix` or a `CCountBlocks`. The client does nothing with the values.

**Parameters:**
- `v: { struct }`
 => If `CFix`
  - `water: int`: ?
  - `invalid: int`: ?
 => If `CCountBlocks`
  - `count: int`: Number of blocks inside the zone (except height 0 ?)
  - `surf: int`: Number of water blocks at height 0

## AShowError

Sent to the client to display an error message in the error pannel (same pannel as FCHK).

**Parameters:**
- `text: String`: text to display


## APong

Sent immediatly after a CPing

**Parameters:**
- `i: int`: inr sent to CPing


## ASetMany

Like `ASet`, but with multiple blocks. See #ASet parameters for more info.

**Parameters:**
- `a: Array<int>`: must be a multiple of 4 `[x,y,z,k,x,y,z,k,...]`
